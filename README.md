# How to run?

- Go to project root

```shell
cd pricing-engine-go
go run cmd/main.go
```

- Then, you can hit `http://localhost:3000/generate_pricing`, to get the calculated rate.
  The payload should be something like,

```json
{
  "date_of_birth": "1999-12-03",
  "insurance_group": 12,
  "licence_held_since": "2015-12-04"
}
```

- You should get response like,

```json
{
  "Pricing": [
    {
      "Seconds": 1800,
      "Rate": 4.2
    },
    {
      "Seconds": 3600,
      "Rate": 7.59
    },
    {
      "Seconds": 7200,
      "Rate": 11.63
    },
    {
      "Seconds": 10800,
      "Rate": 15.37
    },
    {
      "Seconds": 21600,
      "Rate": 19.13
    },
    {
      "Seconds": 43200,
      "Rate": 31.31
    },
    {
      "Seconds": 86400,
      "Rate": 34.05
    },
    {
      "Seconds": 172800,
      "Rate": 50.03
    },
    {
      "Seconds": 259200,
      "Rate": 68.05
    },
    {
      "Seconds": 345600,
      "Rate": 80.14
    }
  ]
}
```

The rate is provided in pounds.

# How to test?
To test, run

```shell
go test ./service/app -coverprofile cover.out
```

It will show the output
```shell
ok  	pricingengine/service/app	0.005s	coverage: 92.8% of statements
```

To find the coverage

```shell
go tool cover -func=cover.out
```

It will show,

```shell
pricingengine/service/app/generate_pricing.go:14:	init			100.0%
pricingengine/service/app/generate_pricing.go:22:	GeneratePricing		72.0%
pricingengine/service/app/premium_factors.go:12:	init			100.0%
pricingengine/service/app/premium_factors.go:41:	SetDriverAgeFactor	100.0%
pricingengine/service/app/premium_factors.go:67:	SetInsuranceGroupFactor	100.0%
pricingengine/service/app/premium_factors.go:84:	SetLicenceLengthFactor	100.0%
pricingengine/service/app/premium_factors.go:103:	CalculateMulFactor	100.0%
pricingengine/service/app/premium_factors.go:109:	CalculateRate		100.0%
pricingengine/service/app/user_info.go:16:		SetDriverAge		100.0%
pricingengine/service/app/user_info.go:32:		SetLicenceLength	100.0%
pricingengine/service/app/user_info.go:42:		SetInsuranceGroup	100.0%
pricingengine/service/app/user_info.go:50:		Validate		100.0%
pricingengine/service/app/utils.go:9:			ceil			100.0%
pricingengine/service/app/utils.go:14:			penceToPounds		100.0%
pricingengine/service/app/utils.go:18:			dateToYears		100.0%
total:							(statements)		92.8%
```

# Note

There was an error in the interval provided for the licence length parameter.
Following is the interval used for the same after discussion with `Razvan Muscalu`

- 0 <= x < 1 -> 1.1
- 1 <= x < 3 -> 1.05
- 3 <= x < 6 -> 1.025
- 6+ -> 0.95

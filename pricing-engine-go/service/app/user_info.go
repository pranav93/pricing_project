package app

import (
	"errors"
)

// UserInfo user's information
type UserInfo struct {
	DriverAgeYears   int8
	DriverAgePrecise float64
	InsuranceGroup   int64
	LicenceLength    float64
}

// SetDriverAge accepts the date of birth, calculates driver's age in years and stores in DriverAgeYears
func (c *UserInfo) SetDriverAge(driverDOB string) error {
	logger.Printf("Driver dob %s\n", driverDOB)
	years, err := dateToYears(driverDOB)
	if err != nil {
		return errors.New("err_user_input: invalid date of birth")
	}
	if years > 150 {
		return errors.New("err_user_input: invalid age")
	}
	c.DriverAgePrecise = years
	c.DriverAgeYears = int8(years)

	return nil
}

// SetLicenceLength calculates driver's licence length in years and stores in LicenceLength
func (c *UserInfo) SetLicenceLength(licenceHeldSince string) error {
	years, err := dateToYears(licenceHeldSince)
	if err != nil || years > 150 {
		return errors.New("err_user_input: invalid date of licence acquisition")
	}
	c.LicenceLength = years
	return nil
}

// SetInsuranceGroup stores the InsuranceGroup
func (c *UserInfo) SetInsuranceGroup(insuranceGroup int64) error {
	if insuranceGroup < 1 {
		return errors.New("err_user_input: invalid insurance group")
	}
	c.InsuranceGroup = insuranceGroup
	return nil
}

func (c *UserInfo) Validate() error {
	if c.LicenceLength > float64(c.DriverAgePrecise-16) {
		return errors.New("err_user_input: invalid licence length")
	}
	return nil
}

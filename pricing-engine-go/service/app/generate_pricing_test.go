package app

import (
	"context"
	"pricingengine"
	"pricingengine/testutils"
	"testing"
	"time"
)

func TestGeneratePricing(t *testing.T) {
	patch := testutils.PatchTime()
	defer patch.Unpatch()

	d := time.Now().Add(time.Second * 60)
	ctx, cancelFunc := context.WithDeadline(context.Background(), d)
	a := App{}

	input := pricingengine.GeneratePricingRequest{
		DateOfBirth:      "1999-12-03",
		InsuranceGroup:   12,
		LicenceHeldSince: "2015-12-04",
	}

	_, err := a.GeneratePricing(ctx, &input)
	// We have already tested calculated rate
	if err != nil {
		cancelFunc()
		t.Fatalf(err.Error())
	}
	cancelFunc()
}

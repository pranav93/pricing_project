package app

import (
	"math"
	"pricingengine/testutils"
	"testing"
)

func TestSetDriverAge(t *testing.T) {
	patch := testutils.PatchTime()
	defer patch.Unpatch()

	userInfo := UserInfo{}
	if err := userInfo.SetDriverAge("1993-01-01"); err != nil {
		t.Fatal(err.Error())
	}
	var expectedAge int8 = 28
	if userInfo.DriverAgeYears != expectedAge {
		t.Errorf("Expected %d got %d\n", expectedAge, userInfo.DriverAgeYears)
	}
}

func TestSetDriverAgeInvalidDOB(t *testing.T) {
	userInfo := UserInfo{}
	expectedError := "err_user_input: invalid date of birth"
	if err := userInfo.SetDriverAge("1993-1-1"); err != nil &&
		err.Error() != expectedError {
		t.Errorf("Expected error to be %s but got %s\n", expectedError, err.Error())
	}
}

func TestSetDriverAgeInvalidAge(t *testing.T) {
	patch := testutils.PatchTime()
	defer patch.Unpatch()

	userInfo := UserInfo{}

	// var threshold int8 = 150
	expectedError := "err_user_input: invalid age"
	if err := userInfo.SetDriverAge("1860-01-01"); err != nil &&
		err.Error() != expectedError {
		t.Errorf("Expected error to be %s but got %s\n", expectedError, err.Error())
	}
}

func TestSetLicenceLength(t *testing.T) {
	patch := testutils.PatchTime()
	defer patch.Unpatch()

	userInfo := UserInfo{}
	if err := userInfo.SetLicenceLength("1993-01-01"); err != nil {
		t.Fatal(err.Error())
	}
	expectedLength := 28.44
	output := userInfo.LicenceLength
	output = math.Round(output*100) / 100
	if output != expectedLength {
		t.Errorf("Expected %f got %f\n", expectedLength, output)
	}
}

func TestSetLicenceLengthInvalidDate(t *testing.T) {
	userInfo := UserInfo{}
	expectedError := "err_user_input: invalid date of licence acquisition"
	if err := userInfo.SetLicenceLength("1993-1-1"); err != nil &&
		err.Error() != expectedError {
		t.Errorf("Expected error to be %s but got %s\n", expectedError, err.Error())
	}
}

func TestSetLicenceLengthInvalidLength(t *testing.T) {
	patch := testutils.PatchTime()
	defer patch.Unpatch()

	userInfo := UserInfo{}
	expectedError := "err_user_input: invalid date of licence acquisition"
	if err := userInfo.SetLicenceLength("1860-01-01"); err != nil &&
		err.Error() != expectedError {
		t.Errorf("Expected error to be %s but got %s\n", expectedError, err.Error())
	}
}

func TestSetInsuranceGroup(t *testing.T) {
	userInfo := UserInfo{}
	if err := userInfo.SetInsuranceGroup(3); err != nil {
		t.Fatalf(err.Error())
	}
	if userInfo.InsuranceGroup != 3 {
		t.Errorf("Expected %d got %d\n", 3, userInfo.InsuranceGroup)
	}
}

func TestSetInsuranceGroupInvalid(t *testing.T) {
	userInfo := UserInfo{}
	expectedError := "err_user_input: invalid insurance group"
	if err := userInfo.SetInsuranceGroup(-3); err != nil && err.Error() != expectedError {
		t.Errorf("Expected error to be %s but got %s\n", expectedError, err.Error())
	}
}

func TestValidate(t *testing.T) {
	userInfo := UserInfo{
		DriverAgePrecise: 20.5,
		LicenceLength:    4,
	}
	if err := userInfo.Validate(); err != nil {
		t.Errorf("Expected to validate successfully, but got %s\n", err.Error())
	}
}

func TestValidateInvalid(t *testing.T) {
	userInfo := UserInfo{
		DriverAgePrecise: 20.5,
		LicenceLength:    6,
	}
	expectedError := "err_user_input: invalid licence length"
	if err := userInfo.Validate(); err.Error() != expectedError {
		t.Errorf("Expected %s, but got %s\n", expectedError, err.Error())
	}
}

package app

import (
	"math"
	"pricingengine/testutils"
	"testing"
)

func TestCeil(t *testing.T) {
	type test struct {
		input     float64
		precision int8
		expected  float64
	}
	tests := []test{
		{input: 123.4325, precision: 2, expected: 123.43},
		{input: 229.3259, precision: 3, expected: 229.326},
		{input: 12.5001, precision: 0, expected: 13},
	}
	for _, v := range tests {
		output := ceil(v.input, v.precision)
		if output != v.expected {
			t.Errorf("Expected %f got %f\n", v.expected, output)
		}
	}
}

func TestPenceToPounds(t *testing.T) {
	type test struct {
		pence  float64
		pounds float64
	}
	tests := []test{
		{pence: 4325, pounds: 43.25},
		{pence: 3259, pounds: 32.59},
		{pence: 5001, pounds: 50.01},
	}

	for _, v := range tests {
		output := penceToPounds(v.pence)
		if output != v.pounds {
			t.Errorf("Expected %f got %f\n", v.pounds, output)
		}
	}
}

func TestDateToYears(t *testing.T) {
	patch := testutils.PatchTime()
	defer patch.Unpatch()

	type test struct {
		date  string
		years float64
	}
	tests := []test{
		{date: "1923-12-23", years: 97.52},
		{date: "1972-06-12", years: 49.01},
		{date: "2010-03-01", years: 11.27},
	}
	for _, v := range tests {
		output, err := dateToYears(v.date)
		if err != nil {
			t.Fatalf(err.Error())
		}

		output = math.Round(output*100) / 100

		if output != v.years {
			t.Errorf("Expected %f got %f\n", v.years, output)
		}
	}
}

func TestDateToYearsWrongDate(t *testing.T) {
	_, err := dateToYears("123214-322-12")
	if err != nil && err.Error() != "invalid date" {
		t.Errorf("Expected error 'invalid date'")
	}
}

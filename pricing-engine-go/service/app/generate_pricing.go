package app

import (
	"context"
	"fmt"
	"log"

	"pricingengine"
	"pricingengine/applogger"
)

var logger *log.Logger

func init() {
	logger = applogger.GetLogger()
}

type App struct{}

// GeneratePricing will calculate how much a 'risk' be priced or if they should
// be denied.
func (a *App) GeneratePricing(
	ctx context.Context, input *pricingengine.GeneratePricingRequest,
) (*pricingengine.GeneratePricingResponse, error) {
	logger.Println(fmt.Sprintf("%#v", input))

	userInfo := UserInfo{}
	if err := userInfo.SetDriverAge(input.DateOfBirth); err != nil {
		return nil, err
	}
	logger.Printf("userInfo.DriverAgeYears %d\n", userInfo.DriverAgeYears)

	if err := userInfo.SetLicenceLength(input.LicenceHeldSince); err != nil {
		return nil, err
	}
	logger.Printf("userInfo.LicenceLength %f\n", userInfo.LicenceLength)

	if err := userInfo.SetInsuranceGroup(input.InsuranceGroup); err != nil {
		return nil, err
	}
	logger.Printf("userInfo.InsuranceGroup %d\n", userInfo.InsuranceGroup)

	if err := userInfo.Validate(); err != nil {
		return nil, err
	}

	premium := Premium{}
	if err := premium.SetDriverAgeFactor(userInfo.DriverAgeYears); err != nil {
		return nil, err
	}
	logger.Printf("driverAgeFactor %f\n", premium.DriverAgeFactor)

	if err := premium.SetInsuranceGroupFactor(userInfo.InsuranceGroup); err != nil {
		return nil, err
	}
	logger.Printf("InsuranceGroupFactor %f\n", premium.InsuranceGroupFactor)

	if err := premium.SetLicenceLengthFactor(userInfo.LicenceLength); err != nil {
		return nil, err
	}
	logger.Printf("LicenceLengthFactor %f\n", premium.LicenceLengthFactor)

	premium.CalculateRate()
	return &pricingengine.GeneratePricingResponse{
		Pricing: premium.CalculatedRate,
	}, nil
}

package app

import (
	"errors"
	"pricingengine"
	"sort"
)

var baseRateMap = map[int]int{}
var rateKeys = []int{}

func init() {
	baseRateMap = map[int]int{
		1800:   273,
		3600:   493,
		7200:   755,
		10800:  998,
		21600:  1242,
		43200:  2033,
		86400:  2211,
		172800: 3249,
		259200: 4419,
		345600: 5204,
	}

	for k := range baseRateMap {
		rateKeys = append(rateKeys, k)
	}
	sort.Ints(rateKeys)
}

// Premium is a struct that stores multiplication factors that need to apply to base rate
type Premium struct {
	DriverAgeFactor      float64
	InsuranceGroupFactor float64
	LicenceLengthFactor  float64
	CalculatedRate       []pricingengine.CalculatedPricing
}

// SetDriverAgeFactor sets the DriverAgeFactor as per the age of the driver
func (p *Premium) SetDriverAgeFactor(driverAge int8) error {
	logger.Printf("driverAge %d\n", driverAge)
	if driverAge < 18 {
		return errors.New("err_user_input: invalid driver age")
	}

	driverAgeFactorMap := map[int8]float64{
		18: 1.54,
		19: 1.52,
		20: 1.44,
		21: 1.4,
		22: 1.37,
		23: 1.36,
		24: 1.36,
		25: 1.1,
	}

	if factor, ok := driverAgeFactorMap[driverAge]; !ok {
		p.DriverAgeFactor = 1
	} else {
		p.DriverAgeFactor = factor
	}
	return nil
}

// SetInsuranceGroupFactor sets the InsuranceGroupFactor as per the chosen insurance group
func (p *Premium) SetInsuranceGroupFactor(insuranceGroup int64) error {
	switch {
	case insuranceGroup >= 1 && insuranceGroup <= 8:
		p.InsuranceGroupFactor = 1
		return nil
	case insuranceGroup >= 9 && insuranceGroup <= 16:
		p.InsuranceGroupFactor = 1.073
		return nil
	case insuranceGroup >= 17 && insuranceGroup <= 35:
		p.InsuranceGroupFactor = 1.12
		return nil
	default:
		return errors.New("err_user_input: invalid insurance group")
	}
}

// SetLicenceLengthFactor sets the LicenceLengthFactor as per the length of the licence duration
func (p *Premium) SetLicenceLengthFactor(licenceLength float64) error {
	switch {
	case licenceLength >= 0 && licenceLength < 1:
		p.LicenceLengthFactor = 1.1
		return nil
	case licenceLength >= 1 && licenceLength < 3:
		p.LicenceLengthFactor = 1.05
		return nil
	case licenceLength >= 3 && licenceLength < 6:
		p.LicenceLengthFactor = 1.025
		return nil
	case licenceLength >= 6:
		p.LicenceLengthFactor = 0.95
		return nil
	default:
		return errors.New("err_user_input: invalid licence length")
	}
}

func (p *Premium) CalculateMulFactor() float64 {
	return ceil(p.DriverAgeFactor*p.InsuranceGroupFactor*p.LicenceLengthFactor, 3)
}

// CalculateRate calculates the rate using the various multiplication factors
// such as driver's age, insurance group selected, and licence length
func (p *Premium) CalculateRate() {
	factor := p.CalculateMulFactor()
	logger.Printf("ceil(factor, 3) %f\n", factor)

	calculatedRate := []pricingengine.CalculatedPricing{}
	for _, duration := range rateKeys {
		logger.Println(duration, baseRateMap[duration])
		calculatedRate = append(
			calculatedRate,
			pricingengine.CalculatedPricing{
				Seconds: duration,
				Rate:    penceToPounds(ceil(float64(baseRateMap[duration])*factor, 0)),
			},
		)
	}
	logger.Println(calculatedRate)
	p.CalculatedRate = calculatedRate
}

package app

import (
	"errors"
	"math"
	"time"
)

func ceil(value float64, precision int8) float64 {
	factor := math.Pow(10, float64(precision))
	return math.Round(value*factor) / factor
}

func penceToPounds(pence float64) float64 {
	return pence / 100
}

func dateToYears(date string) (float64, error) {
	dateTime, err := time.Parse("2006-01-02", date)
	logger.Printf("datetime %s\n", dateTime.String())

	if err != nil {
		return 0, errors.New("invalid date")
	}
	DriverDOBDuration := time.Since(dateTime)
	logger.Printf("years %s\n", DriverDOBDuration.String())
	logger.Printf("years %f\n", DriverDOBDuration.Seconds()/(86400*365))
	return DriverDOBDuration.Seconds() / (86400 * 365), nil
}

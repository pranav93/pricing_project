package app

import (
	"testing"
)

func TestSetDriverAgeFactor(t *testing.T) {
	type test struct {
		driverAge       int8
		driverAgeFactor float64
	}

	tests := []test{
		{driverAge: 18, driverAgeFactor: 1.54},
		{driverAge: 19, driverAgeFactor: 1.52},
		{driverAge: 20, driverAgeFactor: 1.44},
		{driverAge: 21, driverAgeFactor: 1.4},
		{driverAge: 22, driverAgeFactor: 1.37},
		{driverAge: 23, driverAgeFactor: 1.36},
		{driverAge: 24, driverAgeFactor: 1.36},
		{driverAge: 25, driverAgeFactor: 1.1},
		{driverAge: 26, driverAgeFactor: 1},
	}

	for _, v := range tests {

		premium := Premium{}
		if err := premium.SetDriverAgeFactor(v.driverAge); err != nil {
			t.Fatalf(err.Error())
		}
		if premium.DriverAgeFactor != v.driverAgeFactor {
			t.Errorf(
				"For input %d, Expected %f got %f\n",
				v.driverAge, v.driverAgeFactor, premium.DriverAgeFactor)
		}
	}
}

func TestSetDriverAgeFactorInvalid(t *testing.T) {
	premium := Premium{}
	expectedError := "err_user_input: invalid driver age"
	if err := premium.SetDriverAgeFactor(17); err != nil && err.Error() != expectedError {
		t.Fatalf("Expected error %s got %s\n", expectedError, err.Error())
	}
}

func TestSetInsuranceGroupFactor(t *testing.T) {
	type test struct {
		insuranceGroup       int64
		insuranceGroupFactor float64
	}

	tests := []test{
		{insuranceGroup: 1, insuranceGroupFactor: 1},
		{insuranceGroup: 2, insuranceGroupFactor: 1},
		{insuranceGroup: 3, insuranceGroupFactor: 1},
		{insuranceGroup: 4, insuranceGroupFactor: 1},
		{insuranceGroup: 5, insuranceGroupFactor: 1},
		{insuranceGroup: 6, insuranceGroupFactor: 1},
		{insuranceGroup: 7, insuranceGroupFactor: 1},
		{insuranceGroup: 8, insuranceGroupFactor: 1},
		{insuranceGroup: 9, insuranceGroupFactor: 1.073},
		{insuranceGroup: 10, insuranceGroupFactor: 1.073},
		{insuranceGroup: 11, insuranceGroupFactor: 1.073},
		{insuranceGroup: 12, insuranceGroupFactor: 1.073},
		{insuranceGroup: 13, insuranceGroupFactor: 1.073},
		{insuranceGroup: 14, insuranceGroupFactor: 1.073},
		{insuranceGroup: 15, insuranceGroupFactor: 1.073},
		{insuranceGroup: 16, insuranceGroupFactor: 1.073},
		{insuranceGroup: 17, insuranceGroupFactor: 1.12},
		{insuranceGroup: 18, insuranceGroupFactor: 1.12},
		{insuranceGroup: 19, insuranceGroupFactor: 1.12},
		{insuranceGroup: 20, insuranceGroupFactor: 1.12},
		{insuranceGroup: 21, insuranceGroupFactor: 1.12},
		{insuranceGroup: 22, insuranceGroupFactor: 1.12},
		{insuranceGroup: 23, insuranceGroupFactor: 1.12},
		{insuranceGroup: 24, insuranceGroupFactor: 1.12},
		{insuranceGroup: 25, insuranceGroupFactor: 1.12},
		{insuranceGroup: 26, insuranceGroupFactor: 1.12},
		{insuranceGroup: 27, insuranceGroupFactor: 1.12},
		{insuranceGroup: 28, insuranceGroupFactor: 1.12},
		{insuranceGroup: 29, insuranceGroupFactor: 1.12},
		{insuranceGroup: 30, insuranceGroupFactor: 1.12},
		{insuranceGroup: 31, insuranceGroupFactor: 1.12},
		{insuranceGroup: 32, insuranceGroupFactor: 1.12},
		{insuranceGroup: 33, insuranceGroupFactor: 1.12},
		{insuranceGroup: 34, insuranceGroupFactor: 1.12},
		{insuranceGroup: 35, insuranceGroupFactor: 1.12},
	}

	for _, v := range tests {
		premium := Premium{}
		if err := premium.SetInsuranceGroupFactor(v.insuranceGroup); err != nil {
			t.Fatalf(err.Error())
		}
		if premium.InsuranceGroupFactor != v.insuranceGroupFactor {
			t.Errorf(
				"For input %d, Expected %f got %f\n",
				v.insuranceGroup, v.insuranceGroupFactor, premium.InsuranceGroupFactor)
		}
	}
}

func TestSetInsuranceGroupFactorInvalid(t *testing.T) {
	premium := Premium{}
	expectedError := "err_user_input: invalid insurance group"
	if err := premium.SetInsuranceGroupFactor(-1); err != nil && err.Error() != expectedError {
		t.Fatalf("Expected error %s got %s\n", expectedError, err.Error())
	}
}

func TestSetLicenceLengthFactor(t *testing.T) {
	type test struct {
		licenceLength       float64
		licenceLengthFactor float64
	}

	tests := []test{
		{licenceLength: 0.5, licenceLengthFactor: 1.1},
		{licenceLength: 0.9, licenceLengthFactor: 1.1},
		{licenceLength: 1.0, licenceLengthFactor: 1.05},
		{licenceLength: 1.01, licenceLengthFactor: 1.05},
		{licenceLength: 2.0, licenceLengthFactor: 1.05},
		{licenceLength: 3.0, licenceLengthFactor: 1.025},
		{licenceLength: 3.1, licenceLengthFactor: 1.025},
		{licenceLength: 4.2, licenceLengthFactor: 1.025},
		{licenceLength: 5.99, licenceLengthFactor: 1.025},
		{licenceLength: 6, licenceLengthFactor: 0.95},
		{licenceLength: 6.1, licenceLengthFactor: 0.95},
		{licenceLength: 6.5, licenceLengthFactor: 0.95},
		{licenceLength: 7, licenceLengthFactor: 0.95},
		{licenceLength: 8, licenceLengthFactor: 0.95},
		{licenceLength: 9, licenceLengthFactor: 0.95},
	}

	for _, v := range tests {

		premium := Premium{}
		if err := premium.SetLicenceLengthFactor(v.licenceLength); err != nil {
			t.Fatalf(err.Error())
		}
		if premium.LicenceLengthFactor != v.licenceLengthFactor {
			t.Errorf(
				"For input %f, Expected %f got %f\n",
				v.licenceLength, v.licenceLengthFactor, premium.LicenceLengthFactor)
		}
	}
}

func TestSetLicenceLengthFactorInvalid(t *testing.T) {
	premium := Premium{}
	expectedError := "err_user_input: invalid licence length"
	if err := premium.SetLicenceLengthFactor(-1); err != nil && err.Error() != expectedError {
		t.Fatalf("Expected error %s got %s\n", expectedError, err.Error())
	}
}

func TestCalculateMulFactor(t *testing.T) {
	premium := Premium{}
	premium.DriverAgeFactor = 1.52
	premium.InsuranceGroupFactor = 1.12
	premium.LicenceLengthFactor = 1.05

	factor := premium.CalculateMulFactor()

	if factor != 1.788 {
		t.Errorf("Expected %f got %f\n", 1.788, factor)
	}
}

func TestCalculateRate(t *testing.T) {
	expectedRate := map[int]float64{
		1800:   4.88,
		3600:   8.81,
		7200:   13.5,
		10800:  17.84,
		21600:  22.21,
		43200:  36.35,
		86400:  39.53,
		172800: 58.09,
		259200: 79.01,
		345600: 93.05}

	premium := Premium{}
	premium.DriverAgeFactor = 1.52
	premium.InsuranceGroupFactor = 1.12
	premium.LicenceLengthFactor = 1.05

	premium.CalculateRate()

	rate := premium.CalculatedRate

	for _, v := range rate {
		if expectedVal, ok := expectedRate[v.Seconds]; ok {
			if expectedVal != v.Rate {
				t.Errorf("Expected %f got %f\n", expectedVal, v.Rate)
			}
		} else {
			t.Fatalf("Not found rate key %d\n", v.Seconds)
		}
	}
}

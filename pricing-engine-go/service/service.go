package service

import (
	"log"
	"net/http"
	"time"

	"pricingengine/service/app"
	"pricingengine/service/rpc"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

// Start begins a chi-Mux'd net/http server on port 3000
func Start() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middleware.Timeout(5 * time.Second))

	rpc := rpc.RPC{
		App: &app.App{},
	}

	r.Post("/generate_pricing", rpc.GeneratePricing)
	if err := http.ListenAndServe("localhost:3000", r); err != nil {
		log.Println(err.Error())
		log.Fatalln("Unable to listen and serve")
	}
}

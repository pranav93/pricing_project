module pricingengine

go 1.14

require (
	bou.ke/monkey v1.0.2
	github.com/go-chi/chi v4.1.2+incompatible
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/text v0.3.5 // indirect
)

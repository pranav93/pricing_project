package pricingengine

// GeneratePricingRequest is used for generate pricing requests, it holds the
// inputs that are used to provide pricing for a given user.
type GeneratePricingRequest struct {
	// DateOfBirth provides user's date of birth
	DateOfBirth string `json:"date_of_birth"`
	// InsuranceGroup provides chosen insurance group
	InsuranceGroup int64 `json:"insurance_group"`
	// LicenceHeldSince tells since when the user has held the licence
	LicenceHeldSince string `json:"licence_held_since"`
}

// GeneratePricingResponse provides the struct in which the client gets the
// response
type GeneratePricingResponse struct {
	// Pricing is slice of struct that provides the pricing information
	Pricing []CalculatedPricing
}

// CalculatedPricing provides the pricing information for a duration
type CalculatedPricing struct {
	//Seconds Duration in seconds
	Seconds int
	// Rate in pounds
	Rate float64
}

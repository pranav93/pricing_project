package testutils

import (
	"time"

	"bou.ke/monkey"
)

func PatchTime() *monkey.PatchGuard {
	frozenTime := time.Date(2021, time.June, 5, 0, 0, 0, 0, time.UTC)
	patch := monkey.Patch(time.Now, func() time.Time { return frozenTime })
	return patch
}

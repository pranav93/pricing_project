package applogger

import (
	"log"
	"os"
)

var logger *log.Logger

func init() {
	logger = log.New(os.Stdout, "[pricingengine]", log.Ldate|log.Ltime|log.Lshortfile)
}

func GetLogger() *log.Logger {
	return logger
}
